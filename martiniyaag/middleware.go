package martiniyaag

import (
	"net/http"

	"gitee.com/ostaer/yaag/middleware"
	"gitee.com/ostaer/yaag/yaag"
	"gitee.com/ostaer/yaag/yaag/models"
	"github.com/go-martini/martini"
)

func Document(c martini.Context, w http.ResponseWriter, r *http.Request) {
	if !yaag.IsOn() {
		c.Next()
		return
	}
	apiCall := models.ApiCall{}
	writer := middleware.NewResponseRecorder(w)
	c.MapTo(writer, (*http.ResponseWriter)(nil))
	middleware.Before(&apiCall, r)
	c.Next()
	middleware.After(&apiCall, writer, r)
}
